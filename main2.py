'''

    INSTALAR LIBRERIA

        pip3 install --upgrade google-cloud-datastore

    ejecutar

        python3 main2.py

'''
# Imports the Google Cloud client library
from google.cloud import datastore
import os
import json
import random
import datetime

pkjson = "/media/power/DATA/CODE_SOURCE/PycharmProjects/googleCloud/datastore/p01/My Project-46b8157d25f1.json"
pkjson2 = "/media/power/DATA/CODE_SOURCE/PycharmProjects/googleCloud/datastore/p01/My Project-c4d26b4915d5.json"
pkjson3 = "My Project-df7d559a2789.json"
'''
export GOOGLE_APPLICATION_CREDENTIALS="/media/power/DATA/CODE_SOURCE/PycharmProjects/googleCloud/datastore/p01/My Project-df7d559a2789.json"
'''

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = pkjson3



# Instantiates a client
datastore_client = datastore.Client()

kind = "regs"

# CAMBIAR VALOR PARA ENVIAR DATOS n_regs == 10 x n
n_regs = 0


"""
    CREACION DE ENTIDADES
"""
print("\n\n\t\tENVIO DATOS\n\n")

for i in range(n_regs):
    regs = list()
    for f in range(10):
        reg = datastore.Entity(key=datastore_client.key(kind))

        reg.update({
            "T": random.random() * 100,
            "HR": random.random() * 100,
            "Time": datetime.datetime.now()
        })
        print(reg)
        regs.append(reg)

    datastore_client.put_multi(regs)


"""
    CONSULTAR ENTIDADES
"""
q = datastore_client.query(kind=kind)
q.add_filter("HR", "<", 20.0)
# q.order = ["-T"]

print("\n\n\t\tLEER DATOS\n\n")

r = list(q.fetch())
for i in r:
    print(i)
