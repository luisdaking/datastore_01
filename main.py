# Imports the Google Cloud client library
from google.cloud import datastore
from oauth2client.service_account import ServiceAccountCredentials
import json

pkjson = "/media/power/DATA/CODE_SOURCE/PycharmProjects/googleCloud/datastore/p01/My Project-46b8157d25f1.json"
pkjson2 = "/media/power/DATA/CODE_SOURCE/PycharmProjects/googleCloud/datastore/p01/My Project-c4d26b4915d5.json"
pkjson3 = "/media/power/DATA/CODE_SOURCE/PycharmProjects/googleCloud/datastore/p01/My Project-df7d559a2789.json"
f = open(pkjson, "r")
fjson = json.load(f)
'''
export GOOGLE_APPLICATION_CREDENTIALS="/media/power/DATA/CODE_SOURCE/PycharmProjects/googleCloud/datastore/p01/My Project-df7d559a2789.json"
'''

# Instantiates a client
datastore_client = datastore.Client.from_service_account_json(pkjson3)


# The kind for the new entity
kind = 'Task'
# The name/ID for the new entity
name = 'sampletask1'
# The Cloud Datastore key for the new entity
task_key = datastore_client.key(kind, name)

# Prepares the new entity
task = datastore.Entity(key=task_key)
task['description'] = 'Buy milk'

# Saves the entity
datastore_client.put(task)

print('Saved {}: {}'.format(task.key.name, task['description']))
